#include "Arduino.h"

#include "src/FechaduraDigital.h"
#include "src/implementations/BuzzerImp.h"
#include "src/implementations/DisplayImp.h"
#include "src/implementations/KeypadImp.h"
#include "src/implementations/LedGreenImp.h"
#include "src/implementations/LedRedImp.h"
#include "src/implementations/LoggerImp.h"
#include "src/implementations/RelayImp.h"
#include "src/implementations/RFIDImp.h"
#include "src/implementations/StorageImp.h"
#include "src/implementations/UtilsImp.h"

BuzzerImp 			* buzzer;
FechaduraDigital 	* fechaduraDigital;
DisplayImp 			* display;
KeypadImp 			* keypad;
LedGreenImp 		* ledGreen;
LedRedImp 			* ledRed;
RelayImp 			* relay;
RFIDImp 			* rfid;
StorageImp 			* storage;
LoggerImp 			* logger;
UtilsImp 			* utils;

void setup()
{
	buzzer 			= new BuzzerImp();
	display 		= new DisplayImp();
	keypad 			= new KeypadImp();
	ledGreen 		= new LedGreenImp();
	ledRed 			= new LedRedImp();
	relay 			= new RelayImp();
	rfid 			= new RFIDImp();
	storage 		= new StorageImp();
	logger 			= new LoggerImp();
	utils 			= new UtilsImp();

	logger->logFreeRam();

	fechaduraDigital 	= new FechaduraDigital(keypad, display, rfid, storage, relay, ledGreen, ledRed, buzzer, logger, utils);

}

void loop()
{
	fechaduraDigital->loop();
}
