#ifndef FD_ABSTRACTION_LAYER_STORAGE_H
#define FD_ABSTRACTION_LAYER_STORAGE_H

#include <string.h>
#include "../interfaces/IStorage.h"

class StorageImp: public IStorage {

	public:
		StorageImp();
		virtual ~StorageImp();

		bool isMaster(const char * tag);
		bool isUser(const char * tag);
		int findUser(const char * tag);
		int addUser(const char * tag, const char * inputPwd);
		void removeUser(const char * tag);
		void removeUsers();
		int getUserSlotAvailable();
		bool validateAccess(const char * tag, const char * password);

	private:
		static const int MAX_USERS = 3;
		static const int MIN_PWD_LEN = 3;
		static const int MAX_PWD_LEN = 7;
		static const int TAG_SIZE = 11;

		const char * UID_MASTER = "2085faa7f8";
		const char * PWD_MASTER = "456";
		char uidEmpty[TAG_SIZE];
		char uidUsers[MAX_USERS][TAG_SIZE];
		char pwdUsers[MAX_USERS][MAX_PWD_LEN];
};

#endif
