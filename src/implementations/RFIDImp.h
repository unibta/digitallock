#ifndef FD_ABSTRACTION_LAYER_RFID_H
#define FD_ABSTRACTION_LAYER_RFID_H

#include "Arduino.h"
#include "../../libraries/RFID/RFID.h"
#include "../interfaces/IRFID.h"

class RFIDImp: public IRFID {
	public:
		RFIDImp();
		virtual ~RFIDImp();

		void readTag(char * buf, unsigned int bufsize);

	private:
		const int RFID_PIN_SDA 	= 10;
		const int RFID_PIN_RST 	= 9;
		const char * RFID_EMPTY	= "None";

		RFID * rfid;
};

#endif
