#ifndef FD_ABSTRACTION_LAYER_AUTILS_H
#define FD_ABSTRACTION_LAYER_AUTILS_H

#include "Arduino.h"
#include "../interfaces/IUtils.h"

class UtilsImp: public IUtils {
	public:
		UtilsImp();
		virtual ~UtilsImp();

		void delay(unsigned long ms);
		bool isDigit(const char c);
		unsigned long millis();
};


#endif
