#include "BuzzerImp.h"

BuzzerImp::BuzzerImp() {
	switchable = new Switchable(PIN_SIGNAL);
}

BuzzerImp::~BuzzerImp() {
	delete switchable;
}

void BuzzerImp::on() {
	switchable->on();
}

void BuzzerImp::off() {
	switchable->off();
}
