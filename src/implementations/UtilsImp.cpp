#include "UtilsImp.h"

UtilsImp::UtilsImp() {

}

UtilsImp::~UtilsImp() {

}

void UtilsImp::delay(unsigned long ms) {
	Arduino_h::delay(ms);
}

bool UtilsImp::isDigit(const char c) {
	return Arduino_h::isDigit(c);
}

unsigned long UtilsImp::millis() {
	return Arduino_h::millis();
}
