#include "KeypadImp.h"

KeypadImp::KeypadImp() {
	keypad = new Keypad( makeKeymap(KEYS), ROW_PINS, COL_PINS, ROWS, COLS );
}

KeypadImp::~KeypadImp() {

}

char KeypadImp::getKey() {
	return keypad->getKey();
}
