#include "StorageImp.h"

StorageImp::StorageImp() {
	memset(uidEmpty, '\0', TAG_SIZE);
	for(int i=0; i<MAX_USERS; ++i) {
		memset(uidUsers[i], '\0', TAG_SIZE);
		memset(pwdUsers[i], '\0', MAX_PWD_LEN);
	}
}

StorageImp::~StorageImp() {

}

bool StorageImp::isMaster(const char * tag){
	return strncmp(UID_MASTER, tag, TAG_SIZE) == 0;
}

bool StorageImp::isUser(const char * tag){
	return findUser(tag)>=0;
}

int StorageImp::findUser(const char * tag){
	for (int i = 0; i < MAX_USERS; ++i) {
		if(strncmp(tag, uidUsers[i], TAG_SIZE) == 0) {
			return i;
		}
	}
	return -1;
}

int StorageImp::addUser(const char * tag, const char * inputPwd) {

	// avoid duplication
	if(findUser(tag)>=0 || isMaster(tag))
		return -1;

	// add user and return the new index
	int index = getUserSlotAvailable();
	strcpy(uidUsers[index], tag);
	strcpy(pwdUsers[index], inputPwd);
	return index;
}

void StorageImp::removeUser(const char * tag){
	int index = findUser(tag);
	memset(uidUsers[index], '\0', TAG_SIZE);
	memset(pwdUsers[index], '\0', MAX_PWD_LEN);
}

void StorageImp::removeUsers(){
	for(int i=0; i<MAX_USERS; ++i) {
		memset(uidUsers[i], '\0', TAG_SIZE);
		memset(pwdUsers[i], '\0', MAX_PWD_LEN);
	}
}

int StorageImp::getUserSlotAvailable(){
	for (int i = 0; i < MAX_USERS; ++i) {
		if(strncmp(uidUsers[i], uidEmpty, TAG_SIZE) == 0) {
			return i;
		}
	}
	return -1;
}

bool StorageImp::validateAccess(const char * tag, const char * password){
	if(strncmp(tag, uidEmpty, TAG_SIZE) == 0) {
		return false;
	} else if(isMaster(tag)) {
		if(strncmp(password, PWD_MASTER, MAX_PWD_LEN) == 0) {
			return true;
		} else {
			return false;
		}
	} else {
		int userIndex = findUser(tag);
		if(strncmp(password, pwdUsers[userIndex], MAX_PWD_LEN) == 0) {
			return true;
		} else {
			return false;
		}
	}
}
