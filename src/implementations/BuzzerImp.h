#ifndef FD_ABSTRACTION_LAYER_BUZZER_H
#define FD_ABSTRACTION_LAYER_BUZZER_H

#include "Arduino.h"
#include "../interfaces/IBuzzer.h"
#include "../../libraries/Switchable/Switchable.h"

class BuzzerImp: public IBuzzer {
	public:
		BuzzerImp();
		virtual ~BuzzerImp();

		void on();
		void off();

	private:
		const int PIN_SIGNAL	= A1;
		Switchable * switchable;
};

#endif

