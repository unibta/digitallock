#ifndef FD_ABSTRACTION_LAYER_LEDRED_H
#define FD_ABSTRACTION_LAYER_LEDRED_H

#include "Arduino.h"
#include "../interfaces/ILedRed.h"
#include "../../libraries/Switchable/Switchable.h"

class LedRedImp: public ILedRed {
	public:
		LedRedImp();
		virtual ~LedRedImp();

		void on();
		void off();

	private:
		const int PIN_SIGNAL	= A2;

		Switchable * switchable;
};

#endif

