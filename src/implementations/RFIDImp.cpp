#include "RFIDImp.h"

RFIDImp::RFIDImp() {
	rfid = new RFID(RFID_PIN_SDA,RFID_PIN_RST);
	rfid->init();
}

RFIDImp::~RFIDImp() {
	delete rfid;
}

void RFIDImp::readTag(char * buf, unsigned int bufsize) {

	// clear buffer
	memset(buf, '\0', bufsize);

	// read new tag
	String str = rfid->readTag();

	// convert to char string and set into buffer
	if(str == RFID_EMPTY) {
		strncpy(buf, RFID_EMPTY, 4);
	} else {
		str.toCharArray(buf, bufsize);
	}
}
