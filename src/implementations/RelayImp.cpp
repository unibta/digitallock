#include "RelayImp.h"

RelayImp::RelayImp() {
	switchable = new Switchable(PIN_SIGNAL, false);
}

RelayImp::~RelayImp() {
	delete switchable;
}

void RelayImp::on() {
	switchable->on();
}

void RelayImp::off() {
	switchable->off();
}
