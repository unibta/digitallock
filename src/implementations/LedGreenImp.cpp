#include "LedGreenImp.h"

LedGreenImp::LedGreenImp() {
	switchable = new Switchable(PIN_SIGNAL);
}

LedGreenImp::~LedGreenImp() {
	delete switchable;
}

void LedGreenImp::on() {
	switchable->on();
}

void LedGreenImp::off() {
	switchable->off();
}
