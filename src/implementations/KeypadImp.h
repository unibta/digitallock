#ifndef FD_ABSTRACTION_LAYER_KEYPAD_H
#define FD_ABSTRACTION_LAYER_KEYPAD_H

#include "Arduino.h"
#include "../interfaces/IKeypad.h"
#include <Keypad.h>

class KeypadImp: public IKeypad {
	public:
		KeypadImp();
		virtual ~KeypadImp();

		char getKey();

	private:
		static const int ROWS = 4;
		static const int COLS = 3;

		byte ROW_PINS[ROWS] = {5, 6, 7, 8};
		byte COL_PINS[COLS] = {2, 3, 4};
		char KEYS[ROWS][COLS] = {
		    {'1','2','3'},
		    {'4','5','6'},
		    {'7','8','9'},
		    {'*','0','#'}
		};

		Keypad * keypad;
};

#endif
