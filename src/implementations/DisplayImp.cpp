#include "DisplayImp.h"

DisplayImp::DisplayImp() {
	lcd = new LiquidCrystal_I2C(LCD_ADDRESS, LCD_COLUMNS, LCD_ROWS);
	lcd->init();
	lcd->backlight();
}

DisplayImp::~DisplayImp() {}

void DisplayImp::print(const char * line1, const char * line2) {
	clear();
	lcd->print(line1);
	lcd->setCursor(0,1);
	lcd->print(line2);
}

void DisplayImp::print(const char * line1, const char line2) {
	clear();
	lcd->print(line1);
	lcd->setCursor(0,1);
	lcd->print(line2);
}

void DisplayImp::clear() {
	lcd->clear();
	delay(LCD_CLEAR_DELAY);
}
