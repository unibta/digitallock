#ifndef FD_ABSTRACTION_LAYER_LEDGREEN_H
#define FD_ABSTRACTION_LAYER_LEDGREEN_H

#include "Arduino.h"
#include "../interfaces/ILedGreen.h"
#include "../../libraries/Switchable/Switchable.h"

class LedGreenImp: public ILedGreen {
	public:
		LedGreenImp();
		virtual ~LedGreenImp();

		void on();
		void off();
	private:
		const int PIN_SIGNAL	= A3;

		Switchable * switchable;
};

#endif

