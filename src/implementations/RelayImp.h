#ifndef FD_ABSTRACTION_LAYER_RELAY_H
#define FD_ABSTRACTION_LAYER_RELAY_H

#include "Arduino.h"
#include "../interfaces/IRelay.h"
#include "../../libraries/Switchable/Switchable.h"

class RelayImp: public IRelay {
	public:
		RelayImp();
		virtual ~RelayImp();

		void on();
		void off();

	private:
		const int PIN_SIGNAL = A0;

		Switchable * switchable;
};

#endif

