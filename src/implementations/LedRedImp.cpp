#include "LedRedImp.h"

LedRedImp::LedRedImp() {
	switchable = new Switchable(PIN_SIGNAL);
}

LedRedImp::~LedRedImp() {
	delete switchable;
}

void LedRedImp::on() {
	switchable->on();
}

void LedRedImp::off() {
	switchable->off();
}
