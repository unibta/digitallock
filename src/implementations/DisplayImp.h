#ifndef FD_ABSTRACTION_LAYER_DISPLAY_H
#define FD_ABSTRACTION_LAYER_DISPLAY_H

#include "Arduino.h"
#include <LiquidCrystal_I2C.h>

#include "../interfaces/IDisplay.h"

class DisplayImp: public IDisplay {
	public:
		DisplayImp();
		virtual ~DisplayImp();

		void print(const char[], const char[]);
		void print(const char[], const char);
		void clear();

	private:
		const int LCD_ADDRESS = 0x27; // old display 0x3F, new display 0x27
		const int LCD_COLUMNS = 16;
		const int LCD_ROWS = 2;
		const int LCD_CLEAR_DELAY = 100;

		LiquidCrystal_I2C * lcd;
};

#endif
