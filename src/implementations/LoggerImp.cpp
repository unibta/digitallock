#include "LoggerImp.h"

#include "../../libraries/MemoryFree/MemoryFree.h"

LoggerImp::LoggerImp() {
	if(!LOG_ENABLED) return;
    Serial.begin(SERIAL_PORT);
    delay(100);
	Serial.println(F("Log::Enabled"));
}

LoggerImp::~LoggerImp() {
	if(!LOG_ENABLED) return;
	Serial.end();
}

void LoggerImp::logFreeRam() {
	if(!LOG_ENABLED) return;
	Serial.print(F("Free RAM = "));
	Serial.println(freeMemory(), DEC);
}

void LoggerImp::log(const char c[]) {
	if(!LOG_ENABLED) return;
	Serial.print(c);
}

void LoggerImp::log(const char c) {
	if(!LOG_ENABLED) return;
	Serial.print(c);
}

void LoggerImp::log(int num) {
	if(!LOG_ENABLED) return;
	Serial.print(num);
}

void LoggerImp::log(long num) {
	if(!LOG_ENABLED) return;
	Serial.print(num);
}

void LoggerImp::log(double num) {
	if(!LOG_ENABLED) return;
	Serial.print(num);
}

void LoggerImp::log(bool b) {
	if(!LOG_ENABLED) return;
	const char * boolString = (b == true ? "true" : "false");
	Serial.print(boolString);
}

void LoggerImp::logln(const char c[]) {
	if(!LOG_ENABLED) return;
	Serial.println(c);
}

void LoggerImp::logln(const char c) {
	if(!LOG_ENABLED) return;
	Serial.println(c);
}

void LoggerImp::logln(int num) {
	if(!LOG_ENABLED) return;
	Serial.println(num);
}

void LoggerImp::logln(long num) {
	if(!LOG_ENABLED) return;
	Serial.println(num);
}

void LoggerImp::logln(double num) {
	if(!LOG_ENABLED) return;
	Serial.println(num);
}

void LoggerImp::logln(bool b) {
	if(!LOG_ENABLED) return;
	const char * boolString = (b == true ? "true" : "false");
	Serial.println(boolString);
}
