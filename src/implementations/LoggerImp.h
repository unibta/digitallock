#ifndef FD_ABSTRACTION_LAYER_ALOG_H
#define FD_ABSTRACTION_LAYER_ALOG_H

#include "Arduino.h"
#include "../interfaces/ILogger.h"

class LoggerImp: public ILogger {
	public:
		LoggerImp();
		virtual ~LoggerImp();

		void logFreeRam();

		void log(const char c[]);
		void log(const char c);
		void log(int num);
		void log(long num);
		void log(double num);
		void log(bool b);

		void logln(const char c[]);
		void logln(const char c);
		void logln(int num);
		void logln(long num);
		void logln(double num);
		void logln(bool b);

	private:
		const bool LOG_ENABLED = true;
		const int SERIAL_PORT = 9600;
};

#endif
