#include "FechaduraDigital.h"

FechaduraDigital::FechaduraDigital(IKeypad * keypad, IDisplay * display, IRFID * rfid, IStorage * storage, IRelay * relay, ILedGreen * ledGreen, ILedRed * ledRed, IBuzzer * buzzer, ILogger * logger, IUtils * utils) {

	FechaduraDigital::keypad 		= keypad;
	FechaduraDigital::display 		= display;
	FechaduraDigital::rfid 			= rfid;
	FechaduraDigital::storage 		= storage;
	FechaduraDigital::relay 		= relay;
	FechaduraDigital::buzzer 		= buzzer;
	FechaduraDigital::redLed 		= ledRed;
	FechaduraDigital::greenLed 		= ledGreen;
	FechaduraDigital::logger 		= logger;
	FechaduraDigital::utils 		= utils;

	// initializing variables
	currentState 				= waiting_card;
	inputPassword 				= '\0';
	inputMenu 					= '\0';
	menuIndex 					= 0;
	inputPasswordBufferIndex 	= 0;
	menuTimeoutStartedTime 		= 0;
	isMenuTimedOut 				= false;

	// clear input and aux tags
	memset(inputTag, '\0', TAG_SIZE);
	memset(addRemoveAuxInputTag, '\0', TAG_SIZE);
	memset(inputPasswordBuffer, '\0', MAX_PASSWORD_SIZE);
}


void FechaduraDigital::loop() {

	logger->logFreeRam();

	switch(currentState) {
		case waiting_card: 									waitingCardStateAction(); 			break;
		case waiting_password: 								waitingPasswordStateAction(); 		break;
		case user_access: 									userAccessStateAction(); 			break;
		case master_access: 								masterAccessStateAction(); 			break;
		case master_menu: 									masterMenuStateAction(); 								break;
		case master_menu_add_user_waiting_card: 			masterMenuAddUserWaitingCardStateAction();				break;
		case master_menu_add_user_max_slots_reached: 		masterMenuAddUserMaxSlotsReachedStateAction(); 			break;
		case master_menu_add_user_invalid_card: 			masterMenuAddUserInvalidCardStateAction(); 				break;
		case master_menu_add_user_waiting_password: 		masterMenuAddUserWaitingPasswordStateAction(); 			break;
		case master_menu_add_user_success: 					masterMenuAddUserSuccessStateAction(); 					break;
		case master_menu_remove_user_waiting_card: 			masterMenuRemoveUserWaitingCardStateAction(); 			break;
		case master_menu_remove_user_invalid_card: 			masterMenuRemoveUserInvalidCardStateAction(); 			break;
		case master_menu_remove_user_confirm: 				masterMenuRemoveUserConfirmStateAction(); 				break;
		case master_menu_remove_user_success: 				masterMenuRemoveUserSuccessStateAction(); 				break;
		case master_menu_remove_all_users_confirm: 			masterMenuRemoveAllUsersConfirmStateAction(); 			break;
		case master_menu_remove_all_users_success: 			masterMenuRemoveAllUsersSuccessStateAction(); 			break;
		case access_denied_invalid_card: 					accessDeniedInvalidCardStateAction();			break;
		case access_denied_invalid_password: 				accessDeniedInvalidPasswordStateAction(); 		break;
		case access_timed_out: 								accessTimedOutStateAction(); 					break;
	}
}

void FechaduraDigital::clearInputPasswordBuffer() {
	inputPasswordBufferIndex = 0;
	memset(inputPasswordBuffer, '\0', MAX_PASSWORD_SIZE);
}

void FechaduraDigital::scanSuccessEffect() {
	greenLed->on(); buzzer->on();
	utils->delay(100);
	greenLed->off(); buzzer->off();
}

void FechaduraDigital::accessGrantedEffect() {
	greenLed->on(); buzzer->on();
	utils->delay(50);
	greenLed->off(); buzzer->off();
	utils->delay(50);
	greenLed->on(); buzzer->on();
	utils->delay(50);
	greenLed->off(); buzzer->off();
	utils->delay(50);
	greenLed->on(); buzzer->on();
	utils->delay(50);
	greenLed->off(); buzzer->off();
}

void FechaduraDigital::accessDeniedEffect() {
	redLed->on(); buzzer->on();
	utils->delay(100);
	redLed->off(); buzzer->off();
	utils->delay(100);
	redLed->on(); buzzer->on();
	utils->delay(250);
	redLed->off(); buzzer->off();
}

void FechaduraDigital::inputKeyEffect() {
	greenLed->on(); buzzer->on();
	utils->delay(50);
	greenLed->off(); buzzer->off();
}

void FechaduraDigital::timeoutCountEffect() {
	redLed->on(); buzzer->on();
	utils->delay(50);
	redLed->off(); buzzer->off();
}

void FechaduraDigital::waitingCardStateAction() {
	display->print(Language::S_HEADER,Language::S_WAIT_CARD);

	do {
		utils->delay(RFID_READ_DELAY_IN_MILLISECONDS);
		rfid->readTag(inputTag, TAG_SIZE);
	} while(strncmp(inputTag, RFID_EMPTY, 4) == 0);

	if(storage->isMaster(inputTag)) {
		scanSuccessEffect();
		currentState = waiting_password;

	} else if(storage->isUser(inputTag)) {
		scanSuccessEffect();
		currentState 	= waiting_password;

	} else {
		currentState = access_denied_invalid_card;
	}
}

void FechaduraDigital::waitingPasswordStateAction() {
	if(storage->isMaster(inputTag)) {
		display->print(Language::S_WELCOME_M,Language::S_WAIT_PWD);
	} else {
		display->print(Language::S_WELCOME,Language::S_WAIT_PWD);
	}

	clearInputPasswordBuffer();

	do {
		inputPassword = keypad->getKey();
		if (utils->isDigit(inputPassword) ||  inputPassword == '*' ||  inputPassword == '#')
		{
			if(inputPassword == '*') {
				if(inputPasswordBufferIndex > 0) {
					inputKeyEffect();
					inputPasswordBufferIndex--;
					inputPasswordBuffer[inputPasswordBufferIndex] = '\0';
				}
			}

			else if(inputPassword == '#') {
				inputKeyEffect();
				utils->delay(250);
			}

			else {
				if(inputPasswordBufferIndex < MAX_PASSWORD_SIZE-1) {
					inputKeyEffect();
					inputPasswordBuffer[inputPasswordBufferIndex] = inputPassword;
					inputPasswordBufferIndex++;
				}
			}

			if(inputPasswordBufferIndex == 0) {
				display->print(Language::S_WELCOME_M,Language::S_WAIT_PWD);
			} else {
				char pwd[MAX_PASSWORD_SIZE] = {'\0'};
				for (int i = 0; i < inputPasswordBufferIndex; ++i) {
					pwd[i] = '*';
				}
				display->print(Language::S_APAG_OK,pwd);
			}
		}
	} while(inputPassword != '#');

	if(storage->validateAccess(inputTag, inputPasswordBuffer)) {
		if(storage->isMaster(inputTag)) {
			accessGrantedEffect();
			currentState = master_access;
		} else {
			accessGrantedEffect();
			currentState = user_access;
		}
	} else {
		currentState = access_denied_invalid_password;
	}
}

void FechaduraDigital::userAccessStateAction() {
	display->print(Language::S_ACC_GRA,Language::S_WELCOME);
	relay->on();
	utils->delay(ACCESS_TIMEOUT_IN_MILLISECONDS);
	currentState = access_timed_out;
}

void FechaduraDigital::masterAccessStateAction() {
	display->print(Language::S_ACC_GRA,Language::S_MENU);

	relay->on();

	menuTimeoutStartedTime = utils->millis();

	inputMenu = 0;
	isMenuTimedOut = false;
	do {
		inputMenu = keypad->getKey();

		if(inputMenu == '#') {
			inputKeyEffect();
		}

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > ACCESS_TIMEOUT_IN_MILLISECONDS;
	} while(inputMenu != '#' && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else {
		currentState = master_menu;
	}
}

void FechaduraDigital::masterMenuStateAction() {
	display->print(Language::S_NAV,Language::S_MENU_1);

	menuTimeoutStartedTime = utils->millis();

	menuIndex = 0;
	inputMenu = 0;
	isMenuTimedOut = false;
	do {
		inputMenu = keypad->getKey();

		if(inputMenu == '7' || inputMenu == '9') {
			inputKeyEffect();

			menuTimeoutStartedTime = utils->millis();

			if(inputMenu == '7') {
				if(menuIndex == 0) {
					menuIndex = MAX_MENU_SIZE-1;
				} else {
					menuIndex--;
				}
			} else {
				if(menuIndex == MAX_MENU_SIZE-1) {
					menuIndex = 0;
				} else {
					menuIndex++;
				}
			}

			switch(menuIndex) {
				case 0:
					display->print(Language::S_NAV,Language::S_MENU_1);
					break;
				case 1:
					display->print(Language::S_NAV,Language::S_MENU_2);
					break;
				case 2:
					display->print(Language::S_NAV,Language::S_MENU_3);
					break;
			}
		}

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;

	} while(inputMenu != '*' && inputMenu != '#' && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else {
		switch(inputMenu) {
			case '*':
				inputKeyEffect();
				currentState = master_access;
			break;
			case '#':
				inputKeyEffect();
				switch(menuIndex) {
					case 0:
						if(storage->getUserSlotAvailable()>=0) {
							currentState = master_menu_add_user_waiting_card;
						} else {
							currentState = master_menu_add_user_max_slots_reached;
						}
					break;
					case 1:
						currentState = master_menu_remove_user_waiting_card;
					break;
					case 2:
						currentState = master_menu_remove_all_users_confirm;
					break;
				}
			break;
		}
	}
}

void FechaduraDigital::masterMenuAddUserWaitingCardStateAction() {
	display->print(Language::S_WAIT_CARD,Language::S_CANC);

	menuTimeoutStartedTime = utils->millis();

	inputMenu = 0;
	do {
		utils->delay(RFID_READ_DELAY_IN_MILLISECONDS);
		inputMenu = keypad->getKey();
		rfid->readTag(addRemoveAuxInputTag, TAG_SIZE);

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;
	} while(inputMenu != '*' && strncmp(addRemoveAuxInputTag, RFID_EMPTY, 4) == 0 && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else if (inputMenu == '*') {
		inputKeyEffect();
		currentState = master_access;
	} else {
		if(storage->findUser(addRemoveAuxInputTag)>=0 || storage->isMaster(addRemoveAuxInputTag)) {
			currentState = master_menu_add_user_invalid_card;
		} else {
			currentState = master_menu_add_user_waiting_password;
		}
	}
}

void FechaduraDigital::masterMenuAddUserMaxSlotsReachedStateAction() {
	accessDeniedEffect();
	display->print(Language::S_ERR_ARM_L0,Language::S_ERR_ARM_L1);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::masterMenuAddUserInvalidCardStateAction() {
	accessDeniedEffect();
	display->print(Language::S_INV_CARD,Language::S_ERR_CAD);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::masterMenuAddUserWaitingPasswordStateAction() {
	scanSuccessEffect();
	display->print(Language::S_WAIT_PWD_DEF,Language::S_CANC_OK);

	menuTimeoutStartedTime = utils->millis();

	clearInputPasswordBuffer();
	do {
		inputPassword = keypad->getKey();
		if (utils->isDigit(inputPassword) ||  inputPassword == '*' ||  inputPassword == '#')
		{
			if(inputPassword == '*') {
				if(inputPasswordBufferIndex > 0) {
					inputKeyEffect();
					inputPasswordBuffer[inputPasswordBufferIndex] = 0;
					inputPasswordBufferIndex--;
				}
			}

			else if(inputPassword == '#') {
				inputKeyEffect();
				utils->delay(KEYPAD_TOUCH_EFFECT_DELAY_IN_MILLISECONDS);
			}

			else {
				if(inputPasswordBufferIndex < MAX_PASSWORD_SIZE-1) {
					inputKeyEffect();
					inputPasswordBuffer[inputPasswordBufferIndex] = inputPassword;
					inputPasswordBufferIndex++;
				}
			}

			if(inputPasswordBufferIndex == 0) {
				display->print(Language::S_WAIT_PWD_DEF,Language::S_CANC_OK);
			} else {
				char pwd[MAX_PASSWORD_SIZE] = {'\0'};
				for (int i = 0; i < inputPasswordBufferIndex; ++i) {
					pwd[i] = '*';
				}
				display->print(Language::S_APAG_OK,pwd);
			}
		}

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;
	} while( (inputPassword != '*' || inputPasswordBufferIndex < MIN_PASSWORD_SIZE) && (inputPassword != '#' || inputPasswordBufferIndex < MIN_PASSWORD_SIZE) && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else if( inputPassword == '*' ) {
		inputKeyEffect();
		currentState = master_access;
	} else {
		storage->addUser(addRemoveAuxInputTag, inputPasswordBuffer);
		currentState = master_menu_add_user_success;
	}
}

void FechaduraDigital::masterMenuAddUserSuccessStateAction() {
	accessGrantedEffect();
	display->print(Language::S_SUCCESS,Language::S_SUC_ADD);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::masterMenuRemoveUserWaitingCardStateAction() {
	display->print(Language::S_WAIT_CARD,Language::S_CANC);

	menuTimeoutStartedTime = utils->millis();

	inputMenu = 0;
	do {
		utils->delay(RFID_READ_DELAY_IN_MILLISECONDS);
		inputMenu = keypad->getKey();
		rfid->readTag(addRemoveAuxInputTag, TAG_SIZE);

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;
	} while(inputMenu != '*' && strncmp(addRemoveAuxInputTag, RFID_EMPTY, 4) == 0 && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else if (inputMenu == '*') {
		inputKeyEffect();
		currentState = master_access;
	} else {
		if(storage->findUser(addRemoveAuxInputTag)<0) {
			currentState = master_menu_remove_user_invalid_card;
		} else {
			scanSuccessEffect();
			currentState = master_menu_remove_user_confirm;
		}
	}
}

void FechaduraDigital::masterMenuRemoveUserInvalidCardStateAction() {
	accessDeniedEffect();
	display->print(Language::S_INV_CARD,Language::S_ERR_N_CAD);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::masterMenuRemoveUserConfirmStateAction() {
	display->print(Language::S_CONF_REM_CARD,Language::S_CANC_OK);

	menuTimeoutStartedTime = utils->millis();

	inputMenu = 0;
	do {
		inputMenu = keypad->getKey();
		if (inputMenu == '*' ||  inputMenu == '#') {
			inputKeyEffect();
		}

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;
	} while(inputMenu != '*' && inputMenu != '#' && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else if(inputMenu == '*') {
		currentState = master_access;
	} else {
		storage->removeUser(addRemoveAuxInputTag);
		currentState = master_menu_remove_user_success;
	}
}

void FechaduraDigital::masterMenuRemoveUserSuccessStateAction() {
	accessGrantedEffect();
	display->print(Language::S_SUCCESS,Language::S_SUC_REM);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::masterMenuRemoveAllUsersConfirmStateAction() {
	display->print(Language::S_CONF_REM_CARDS,Language::S_CANC_OK);

	menuTimeoutStartedTime = utils->millis();

	inputMenu = 0;
	do {
		inputMenu = keypad->getKey();
		if (inputMenu == '*' ||  inputMenu == '#') {
			inputKeyEffect();
		}

		isMenuTimedOut = utils->millis() - menuTimeoutStartedTime > MENU_TIMEOUT_IN_MILLISECONDS;
	} while(inputMenu != '*' && inputMenu != '#' && isMenuTimedOut == false);

	if(isMenuTimedOut) {
		currentState = access_timed_out;
	} else if(inputMenu == '*') {
		currentState = master_access;
	} else {
		storage->removeUsers();
		currentState = master_menu_remove_all_users_success;
	}
}

void FechaduraDigital::masterMenuRemoveAllUsersSuccessStateAction() {
	accessGrantedEffect();
	display->print(Language::S_SUCCESS,Language::S_SUC_REMS);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = master_access;
}

void FechaduraDigital::accessDeniedInvalidCardStateAction() {
	accessDeniedEffect();
	display->print(Language::S_ACC_DEN,Language::S_INV_CARD);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = waiting_card;
}

void FechaduraDigital::accessDeniedInvalidPasswordStateAction() {
	accessDeniedEffect();
	display->print(Language::S_ACC_DEN,Language::S_INV_PWD);
	utils->delay(MSG_DELAY_IN_MILLISECONDS);
	currentState = waiting_card;
}

void FechaduraDigital::accessTimedOutStateAction() {
	display->print(Language::S_ACC_EXP,Language::S_TIMEOUT_5);
	timeoutCountEffect();
	utils->delay(1000);

	display->print(Language::S_ACC_EXP,Language::S_TIMEOUT_4);
	timeoutCountEffect();
	utils->delay(1000);

	display->print(Language::S_ACC_EXP,Language::S_TIMEOUT_3);
	timeoutCountEffect();
	utils->delay(1000);

	display->print(Language::S_ACC_EXP,Language::S_TIMEOUT_2);
	timeoutCountEffect();
	utils->delay(1000);

	display->print(Language::S_ACC_EXP,Language::S_TIMEOUT_1);
	timeoutCountEffect();
	utils->delay(1000);

	accessDeniedEffect();
	relay->off();
	currentState = waiting_card;
}
