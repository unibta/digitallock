#ifndef FD_INTERFACES_RFID_H
#define FD_INTERFACES_RFID_H

class IRFID {
	public:
		virtual void readTag(char * buf, unsigned int bufsize);
};

#endif
