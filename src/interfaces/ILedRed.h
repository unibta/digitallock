#ifndef FD_INTERFACES_LEDRED_H
#define FD_INTERFACES_LEDRED_H

class ILedRed {
	public:
		virtual void on();
		virtual void off();
};

#endif

