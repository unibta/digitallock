#ifndef FD_INTERFACES_KEYPAD_H
#define FD_INTERFACES_KEYPAD_H

class IKeypad {
	public:
		virtual char getKey();
};

#endif
