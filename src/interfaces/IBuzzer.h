#ifndef FD_INTERFACES_BUZZER_H
#define FD_INTERFACES_BUZZER_H

class IBuzzer {
	public:
		virtual void on();
		virtual void off();
};

#endif

