#ifndef FD_INTERFACES_DISPLAY_H
#define FD_INTERFACES_DISPLAY_H

class IDisplay {
	public:
    virtual void print(const char[], const char[]);
    virtual void print(const char[], const char);
    virtual void clear();
};

#endif
