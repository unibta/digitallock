#ifndef FD_INTERFACES_RELAY_H
#define FD_INTERFACES_RELAY_H

class IRelay {
	public:
		virtual void on();
		virtual void off();
};

#endif

