#ifndef FD_INTERFACES_UTILS_H
#define FD_INTERFACES_UTILS_H

class IUtils {
	public:
		virtual void delay(unsigned long ms);
		virtual bool isDigit(const char c);
		virtual unsigned long millis();
};

#endif

