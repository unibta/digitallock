#ifndef FD_INTERFACES_LOG_H
#define FD_INTERFACES_LOG_H

class ILogger {
	public:

		virtual void logFreeRam() = 0;

		virtual void log(const char c[]);
		virtual void log(const char c);
		virtual void log(int num);
		virtual void log(long num);
		virtual void log(double num);
		virtual void log(bool b);

		virtual void logln(const char c[]);
		virtual void logln(const char c);
		virtual void logln(int num);
		virtual void logln(long num);
		virtual void logln(double num);
		virtual void logln(bool b);
};

#endif
