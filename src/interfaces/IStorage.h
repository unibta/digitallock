#ifndef FD_INTERFACES_STORAGE_H
#define FD_INTERFACES_STORAGE_H

class IStorage {
	public:
		virtual bool isMaster(const char * tag);
		virtual bool isUser(const char * tag);
		virtual int findUser(const char * tag);
		virtual int addUser(const char * tag, const char * inputPwd);
		virtual void removeUser(const char * tag);
		virtual void removeUsers();
		virtual int getUserSlotAvailable();
		virtual bool validateAccess(const char * tag, const char * password);
};

#endif
