#ifndef FD_INTERFACES_LEDGREEN_H
#define FD_INTERFACES_LEDGREEN_H

class ILedGreen {
	public:
		virtual void on();
		virtual void off();
};

#endif

