#ifndef FECHADURA_DIGITAL_H
#define FECHADURA_DIGITAL_H

#include <string.h>
#include "Language.h"
#include "interfaces/IBuzzer.h"
#include "interfaces/IDisplay.h"
#include "interfaces/IKeypad.h"
#include "interfaces/ILedGreen.h"
#include "interfaces/ILedRed.h"
#include "interfaces/ILogger.h"
#include "interfaces/IRelay.h"
#include "interfaces/IRFID.h"
#include "interfaces/IStorage.h"
#include "interfaces/IUtils.h"

class FechaduraDigital {

	public:

		FechaduraDigital(IKeypad *, IDisplay *, IRFID *, IStorage *, IRelay *, ILedGreen *, ILedRed *, IBuzzer *, ILogger *, IUtils *);
		void loop();

	private:

		static const int MAX_PASSWORD_SIZE 		= 7;
		static const int TAG_SIZE 				= 11;
		static const int MAX_USERS 				= 3;
		static const int MIN_PASSWORD_SIZE 		= 1;
		static const int MSG_DELAY_IN_MILLISECONDS 			= 2000;
		static const int ACCESS_TIMEOUT_IN_MILLISECONDS 	= 8000;
		static const int MENU_TIMEOUT_IN_MILLISECONDS 		= 20000;
		static const int MAX_MENU_SIZE 						= 3;
		static const int LCD_CLEAR_DELAY_IN_MILLISECONDS 	= 100;
		static const int RFID_READ_DELAY_IN_MILLISECONDS 	= 100;
		static const int KEYPAD_TOUCH_EFFECT_DELAY_IN_MILLISECONDS = 250;

		const char * RFID_EMPTY = "None";

		enum AppState {
			waiting_card,
			waiting_password,
			user_access,
			master_access,
			master_menu,
			master_menu_add_user_waiting_card,
			master_menu_add_user_max_slots_reached,
			master_menu_add_user_invalid_card,
			master_menu_add_user_waiting_password,
			master_menu_add_user_success,
			master_menu_remove_user_waiting_card,
			master_menu_remove_user_invalid_card,
			master_menu_remove_user_confirm,
			master_menu_remove_user_success,
			master_menu_remove_all_users_confirm,
			master_menu_remove_all_users_success,
			access_denied_invalid_card,
			access_denied_invalid_password,
			access_timed_out
		};

		IKeypad 	* keypad;
		IDisplay 	* display;
		IRFID 		* rfid;
		IStorage 	* storage;
		IRelay 		* relay;
		IBuzzer 	* buzzer;
		ILedRed 	* redLed;
		ILedGreen 	* greenLed;
		ILogger 	* logger;
		IUtils 		* utils;

		AppState currentState;
		char inputTag[TAG_SIZE];
		char addRemoveAuxInputTag[TAG_SIZE];
		char inputPasswordBuffer[MAX_PASSWORD_SIZE];
		char inputPassword;
		char inputMenu;
		int menuIndex;
		int inputPasswordBufferIndex;
		long menuTimeoutStartedTime;
		bool isMenuTimedOut;

		void clearInputPasswordBuffer();

		void scanSuccessEffect();
		void accessGrantedEffect();
		void accessDeniedEffect();
		void inputKeyEffect();
		void timeoutCountEffect();

		void waitingCardStateAction();
		void waitingPasswordStateAction();
		void userAccessStateAction();
		void masterAccessStateAction();
		void masterMenuStateAction();
		void masterMenuAddUserWaitingCardStateAction();
		void masterMenuAddUserMaxSlotsReachedStateAction();
		void masterMenuAddUserInvalidCardStateAction();
		void masterMenuAddUserWaitingPasswordStateAction();
		void masterMenuAddUserSuccessStateAction();
		void masterMenuRemoveUserWaitingCardStateAction();
		void masterMenuRemoveUserInvalidCardStateAction();
		void masterMenuRemoveUserConfirmStateAction();
		void masterMenuRemoveUserSuccessStateAction();
		void masterMenuRemoveAllUsersConfirmStateAction();
		void masterMenuRemoveAllUsersSuccessStateAction();
		void accessDeniedInvalidCardStateAction();
		void accessDeniedInvalidPasswordStateAction();
		void accessTimedOutStateAction();

};

#endif
