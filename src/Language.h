#ifndef LANGUAGE_H_
#define LANGUAGE_H_

class Language {
	public:
		static constexpr const char * S_HEADER 			= "UNIBTA:BEM VINDO";
		static constexpr const char * S_WAIT_CARD 		= "PASSE UM CARTAO";
		static constexpr const char * S_WAIT_PWD 		= "DIGITE A SENHA";
		static constexpr const char * S_WAIT_PWD_DEF 	= "DEFINA A SENHA";
		static constexpr const char * S_MENU 			= "MENU: DIGITE #";
		static constexpr const char * S_NAV 			= "< 7   [#]   9 >";
		static constexpr const char * S_MENU_1 			= "> ADICIONAR CART";
		static constexpr const char * S_MENU_2 			= "> REMOVER CART";
		static constexpr const char * S_MENU_3 			= "> REMOVER TODOS";
		static constexpr const char * S_ERR_CAD 		= "C. JA CADASTRADO";
		static constexpr const char * S_ERR_N_CAD 		= "C. N. CADASTRADO";
		static constexpr const char * S_ERR_ARM_L0 		= "ARMAZENAMENTO";
		static constexpr const char * S_ERR_ARM_L1 		= "MAXIMO EXCEDIDO!";
		static constexpr const char * S_CONF_REM_CARD 	= "REMOVER CART?";
		static constexpr const char * S_CONF_REM_CARDS 	= "REMOVER TODOS?";
		static constexpr const char * S_SUC_ADD 		= "CART. ADICIONADO";
		static constexpr const char * S_SUC_REM 		= "CART. REMOVIDO";
		static constexpr const char * S_SUC_REMS 		= "CART. REMOVIDOS";
		static constexpr const char * S_SUCCESS			= "SUCESSO!";
		static constexpr const char * S_WELCOME 		= "BEM VINDO(A)";
		static constexpr const char * S_WELCOME_M 		= "BEM VINDO MESTRE";
		static constexpr const char * S_ACC_GRA 		= "ACESSO LIBERADO!";
		static constexpr const char * S_ACC_DEN 		= "ACESSO NEGADO!";
		static constexpr const char * S_ACC_EXP 		= "ACESSO EXPIRADO!";
		static constexpr const char * S_INV_PWD 		= "SENHA INVALIDA";
		static constexpr const char * S_TIMEOUT_5 		= "FECHANDO EM 5 S";
		static constexpr const char * S_TIMEOUT_4 		= "FECHANDO EM 4 S";
		static constexpr const char * S_TIMEOUT_3 		= "FECHANDO EM 3 S";
		static constexpr const char * S_TIMEOUT_2 		= "FECHANDO EM 2 S";
		static constexpr const char * S_TIMEOUT_1 		= "FECHANDO EM 1 S";
		static constexpr const char * S_INV_CARD 		= "CARTAO INVALIDO";
		static constexpr const char * S_CANC 			= "*.CANCELAR";
		static constexpr const char * S_CANC_OK 		= "*.CANCELAR  #.OK";
		static constexpr const char * S_APAG_OK 		= "*.APAGAR  #.OK";
};

#endif
