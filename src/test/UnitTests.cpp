#include "UnitTests.h"


UnitTests::UnitTests(IKeypad * pKeypad, IDisplay * pDisplay, IRFID * pRFID, IStorage * pStorage, IRelay * pRelay, ILedGreen * pLedGreen, ILedRed * pLedRed, IBuzzer * pBuzzer, ILogger * pLog, IUtils * pUtils) {

	UnitTests::m_keypad 	= pKeypad;
	UnitTests::m_display 	= pDisplay;
	UnitTests::m_rfid 		= pRFID;
	UnitTests::m_storage 	= pStorage;
	UnitTests::m_relay 		= pRelay;
	UnitTests::m_buzzer 	= pBuzzer;
	UnitTests::m_redLed 	= pLedRed;
	UnitTests::m_greenLed 	= pLedGreen;
	UnitTests::m_log 		= pLog;
	UnitTests::m_utils 		= pUtils;

	UnitTests::m_inputChar 	= '\0';
}

UnitTests::~UnitTests() {}

void UnitTests::run() {

	Serial.begin(9600);
	delay(DELAY_BETWEEN_TESTS);

	reproduceBipSoundEfx();

	if(testMenu()) {

		//Serial.println("TESTING LOG");
	    testDisplay();
	    testBuzzer();
	    testLedGreen();
		testLedRed();
	    testRelay();
	    testKeypad();
		testRfid();

	    //testStorage();

		Serial.println("ALL TESTS DONE");
		m_display->print("ALL TESTS DONE", "");
		delay(DELAY_BETWEEN_TESTS);
		m_display->clear();
	} else {
		m_display->clear();
	}
}

bool UnitTests::testMenu() {
    Serial.println("TEST COMPONENTS?");
    Serial.println("[*]Clr  [#]Test");
	m_display->print("START TESTS?", "[*]Clr  [#]Test");
	do{
		m_inputChar = m_keypad->getKey();
		if(m_inputChar == '*') {
			reproduceBipSoundEfx();
			Serial.println(F("Removing all users: "));
			m_storage->removeUsers();
			return true;
		} else if(m_inputChar == '#') {
			reproduceBipSoundEfx();
			return true;
		}

	} while(true);
}

void UnitTests::detectDisplaySerial() {
	byte count = 0;

	  Wire.begin();
	  for (byte i = 8; i < 120; i++)
	  {
	    Wire.beginTransmission (i);
	    if (Wire.endTransmission () == 0)
	      {
	      Serial.print ("Found address: ");
	      Serial.print (i, DEC);
	      Serial.print (" (0x");
	      Serial.print (i, HEX);
	      Serial.println (")");
	      count++;
	      delay (1);  // maybe unneeded?
	      } // end of good response
	  } // end of for loop
	  Serial.println ("Done.");
	  Serial.print ("Found ");
	  Serial.print (count, DEC);
	  Serial.println (" device(s).");
}  // end of setup

void UnitTests::testDisplay() {
	Serial.println("TESTING DISPLAY");
	m_display->clear();
    delay(DELAY_BETWEEN_TESTS);
    m_display->print("TESTING DISPLAY", "OK");
    delay(DELAY_BETWEEN_TESTS);
}

void UnitTests::testBuzzer() {
	Serial.println("TESTING BUZZER");
	m_display->print("TESTING BUZZER", "");
	m_buzzer->on();
	delay(100);
	m_buzzer->off();
	delay(DELAY_BETWEEN_TESTS);
}

void UnitTests::testLedRed() {
    Serial.println("TESTING LED RED");
	m_display->print("TESTING LED RED", "");
	m_redLed->on();
	delay(DELAY_BETWEEN_TESTS);
	m_redLed->off();
}

void UnitTests::testLedGreen() {
	Serial.println("TESTING LED GREEN");
	m_display->print("TESTING LED GRN", "");
	m_greenLed->on();
	delay(DELAY_BETWEEN_TESTS);
	m_greenLed->off();
}

void UnitTests::testRelay() {
	Serial.println("TESTING RELAY");
	m_display->print("TESTING RELAY", "");
	m_relay->on();
	delay(DELAY_BETWEEN_TESTS);
	m_relay->off();
}

void UnitTests::testKeypad() {
    Serial.println("TESTING KEYPAD");
    Serial.println("Press # to end");
	m_display->print("TESTING KEYPAD", "Press # to end");

	do{
		m_inputChar = m_keypad->getKey();
		if(m_utils->isDigit(m_inputChar) ||  m_inputChar == '*' ||  m_inputChar == '#') {
			Serial.println(m_inputChar);
			m_display->print("Press # to end", m_inputChar);
		}
	} while(m_inputChar != '#');
}

void UnitTests::testRfid() {
    Serial.println("TESTING RFID");
	m_display->print("TESTING RFID", "Press # to end");

	do {
		m_utils->delay(1000);
		m_rfid->readTag(m_inputTag, TAG_SIZE);
		m_inputChar = m_keypad->getKey();
		if(strncmp(m_inputTag, RFID_EMPTY, 4)!=0) {
			m_display->print("Press # to end", m_inputTag);
			Serial.println(m_inputTag);
		}
	} while(m_inputChar != '#');
}

void UnitTests::testStorage() {
	Serial.println(F("TESTING STORAGE"));

	// find restored or create and find user 1
//	Serial.print(F("Finding user 1 -> slot: "));
//	Serial.println(m_storage->findUser(UID_USER_1));
//
//	Serial.print(F("Adding a new user 1 -> slot: "));
//	Serial.println(m_storage->addUser(UID_USER_1, PWD_USER_1));
//
//	Serial.print(F("Finding user 1 -> slot: "));
//	Serial.println(m_storage->findUser(UID_USER_1));
//
//	Serial.print(F("Validating user 1 with false credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_1, "0000") ? "true" : "false");
//
//	Serial.print(F("Validating user 1 with true credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_1, PWD_USER_1) ? "true" : "false");
//
//	// find restored or create and find user 2
//	Serial.print(F("Finding user 2 -> slot: "));
//	Serial.println(m_storage->findUser(UID_USER_2));
//
//	Serial.print(F("Adding a new user 2 -> slot: "));
//	Serial.println(m_storage->addUser(UID_USER_2, PWD_USER_2));
//
//	Serial.print(F("Finding user 2 -> slot: "));
//	Serial.println(m_storage->findUser(UID_USER_2));
//
//	Serial.print(F("Validating user 2 with false credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_2, "0000") ? "true" : "false");
//
//	Serial.print(F("Validating user 2 with true credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_2, PWD_USER_2) ? "true" : "false");
//
//	// Remove a user and test the accesses
//	Serial.println(F("Removing user 1:"));
//	m_storage->removeUser(UID_USER_1);
//
//	Serial.print(F("Validating user 1 with false credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_1, "0000") ? "true" : "false");
//
//	Serial.print(F("Validating user 1 with true credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_1, PWD_USER_1) ? "true" : "false");
//
//	Serial.print(F("Validating user 2 with false credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_2, "0000") ? "true" : "false");
//
//	Serial.print(F("Validating user 2 with true credentials -> result: "));
//	Serial.println(m_storage->validateAccess(UID_USER_2, PWD_USER_2) ? "true" : "false");

}

void UnitTests::reproduceBipSoundEfx() {
	m_buzzer->on();
	delay(100);
	m_buzzer->off();
}
