#ifndef TEST_UNITTESTS_H_
#define TEST_UNITTESTS_H_

#include <string.h>
#include "Arduino.h"
#include <EEPROM.h>
#include <Wire.h>

#include "../interfaces/IBuzzer.h"
#include "../interfaces/IDisplay.h"
#include "../interfaces/IKeypad.h"
#include "../interfaces/ILedGreen.h"
#include "../interfaces/ILedRed.h"
#include "../interfaces/ILogger.h"
#include "../interfaces/IRelay.h"
#include "../interfaces/IRFID.h"
#include "../interfaces/IStorage.h"
#include "../interfaces/IUtils.h"

class UnitTests {
	public:
		UnitTests(IKeypad *, IDisplay *, IRFID *, IStorage *, IRelay *, ILedGreen *, ILedRed *, IBuzzer *, ILogger *, IUtils *);
		virtual ~UnitTests();

		void run();

	private:

		static const int TAG_SIZE = 11;
		const int DELAY_BETWEEN_TESTS = 2000;
		const char * RFID_EMPTY = "None";
		const char * UID_MASTER = "2085faa7f8";
		const char * PWD_MASTER = "456";
		const char * UID_USER_1 = "ABCDEFGHIJ";
		const char * PWD_USER_1 = "456";
		const char * UID_USER_2 = "KLMNOPQRST";
		const char * PWD_USER_2 = "789";

		IKeypad 	* m_keypad;
		IDisplay 	* m_display;
		IRFID 		* m_rfid;
		IStorage 	* m_storage;
		IRelay 		* m_relay;
		IBuzzer 	* m_buzzer;
		ILedRed 	* m_redLed;
		ILedGreen 	* m_greenLed;
		ILogger 	* m_log;
		IUtils 		* m_utils;

		char m_inputChar;
		char m_inputTag[TAG_SIZE];

		bool testMenu();
		void testBuzzer();
		void testLedRed();
		void testLedGreen();
		void testRelay();
		void testDisplay();
		void testKeypad();
		void testRfid();
		void testStorage();

		void reproduceBipSoundEfx();
		void detectDisplaySerial();
};

#endif
