#include "Switchable.h"

Switchable::Switchable(const uint8_t pin) : m_pin(pin)
{
    // Set pin as output
    pinMode(m_pin, OUTPUT);
    // Start state if off
	off();
}

Switchable::Switchable(const uint8_t pin, bool inverseOutput) : m_pin(pin)
{
	Switchable::is_output_inverted = inverseOutput;

    // Set pin as output
    pinMode(m_pin, OUTPUT);
    // Start state if off
	off();
}

//turn on:
void Switchable::on()
{
	m_state = true;
	digitalWrite(m_pin, is_output_inverted ? LOW : HIGH);
}

//turn off:
void Switchable::off()
{
	m_state = false;
	digitalWrite(m_pin, is_output_inverted ? HIGH : LOW);
}

void Switchable::toggle()
{
	digitalWrite(m_pin, !m_state); //low
	m_state = !m_state;
}

// dim pin
void Switchable::dim(int dimVal)
{
    analogWrite(m_pin, dimVal);
}
		
bool Switchable::getState()
{
    return m_state;
}

void Switchable::setState(bool state)
{
    digitalWrite(m_pin, state);
    m_state = state;
}




