#ifndef _SWITCHABLE_H_
#define _SWITCHABLE_H_

#include <Arduino.h>

//Base class for output that can be switched on/off via single digital pin:
class Switchable  
{
	public:

		// Consturcutor accepts pin number for output
	    Switchable(const uint8_t pin);
		Switchable(const uint8_t, bool inverseOutput);
		
		// Turn pin on
		void on();
	    
	    // Turn pin off
		void off();
		
		// Toggle pin
		void toggle();
		
		// dim pin
		void dim(int dimVal);
		
		// Get current state
		bool getState();
		
		// Set state with bool variable
		void setState(bool state);
	
	private:
	
		const uint8_t m_pin; 	//output pin
		bool m_state;		//current state
		bool is_output_inverted = false;

		void updateOutputState();
};

#endif // _SWITCHABLE_H_
 

